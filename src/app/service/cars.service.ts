import { Car } from '../model/models';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  constructor() {}

  private parse<T>(json: string): T {
    return JSON.parse(json) as T;
  }

  private set(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  private get(key: string): string {
    return localStorage.getItem(key)!;
  }

  public getCars() {
    return this.parse<Car[]>(this.get("cars"));
  }

  public getOriginalCars() {
    return this.parse<Car[]>(this.get("originalCars"));
  }

  public removeCar(carToRemove: Car) {
    let cars = this.getCars().filter(car => car.Id != carToRemove.Id);
    this.set("cars",cars);
  }

  public changeStatus(newStatus: number, carToChangeId: number ) {
    this.set("cars",this.getCars().map(car => {
      car.Status = (car.Id === carToChangeId) ? newStatus : car.Status
      return car
    }));
  }

  public addCar(car: Car) {
    this.set("cars",[...this.getCars(),car])
  }

  public doFilter(filtro: string) {
    if (filtro == "") {
      this.set("cars",this.parse<Car[]>(this.get("originalCars")));
    } else {
      console.log();
      
      this.set("cars",this.getOriginalCars().filter(car => car.Brand.toLocaleLowerCase().includes(filtro.toLocaleLowerCase())));
    }
  }

}
