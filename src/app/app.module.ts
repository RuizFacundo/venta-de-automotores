import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MainComponent } from './modules/main/main.component';
import { LoginComponent } from './modules/login/login.component';
import { HeaderComponent } from './modules/header/header.component';
import { CarsListComponent } from './modules/cars-list/cars-list.component';
import { RegistryComponent } from './modules/registry/registry.component';
import { MercadoPagoDialogComponent } from './modules/dialogs/mercado-pago-dialog/mercado-pago-dialog.component';
import { BankAccountDialogComponent } from './modules/dialogs/bank-account-dialog/bank-account-dialog.component';
import { CreditCardDialogComponent } from './modules/dialogs/credit-card-dialog/credit-card-dialog.component';
import { CarsService } from './service/cars.service';
import { ReserveDialogComponent } from './modules/dialogs/reserve-dialog/reserve-dialog.component';
import { BookedCarsListComponent } from './modules/booked-cars-list/booked-cars-list.component';
import { ImportsModule } from './imports/material-imports';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    HeaderComponent,
    CarsListComponent,
    RegistryComponent,
    MercadoPagoDialogComponent,
    BankAccountDialogComponent,
    CreditCardDialogComponent,
    ReserveDialogComponent,
    BookedCarsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ImportsModule,
    LayoutModule,
  ],
  providers: [CarsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
