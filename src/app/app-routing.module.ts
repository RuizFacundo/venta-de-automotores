import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './modules/login/login.component';
import { MainComponent } from './modules/main/main.component';
import { CarsListComponent } from './modules/cars-list/cars-list.component';
import { RegistryComponent } from './modules/registry/registry.component';
import { BookedCarsListComponent } from './modules/booked-cars-list/booked-cars-list.component';

const routes: Routes = [
  { path: 'registry', component: RegistryComponent},
  { path: '', component: LoginComponent},
  { path: 'main', component: MainComponent,
    children: [
      { path: 'search', component: CarsListComponent },
      { path: 'search-booked', component: BookedCarsListComponent },
      { path: '**', component: LoginComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
