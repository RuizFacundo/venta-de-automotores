export class Car {
    public Brand!: string;
    public Description!: string;
    public Img!: string;
    public Exchange!: boolean;
    public Status!: number;
    public Id!: number;
    public Price!: number;
}

export class User {
    public Email!: string;
    public Pass!: string;
    public UserRole!: number;
    public Name!: string;
}

