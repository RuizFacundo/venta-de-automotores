export enum UserRole {
    BUYER = 0,
    SELLER = 1,
    VISITOR = 2
}