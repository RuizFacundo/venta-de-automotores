export enum CarStatus {
    RESERVED = 0,
    PAID_OUT = 1,
    NOT_RESERVED = 2,
    PARTIALLY_PAID = 3
}