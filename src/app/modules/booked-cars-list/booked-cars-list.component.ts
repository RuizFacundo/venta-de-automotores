import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CarStatus } from 'src/app/model/card-status';
import { Car, User } from 'src/app/model/models';
import { CarsService } from 'src/app/service/cars.service';

@Component({
  selector: 'app-booked-cars-list',
  templateUrl: './booked-cars-list.component.html',
  styleUrls: ['./booked-cars-list.component.css']
})
export class BookedCarsListComponent implements OnInit {

  carsToSale!: Car[];
  user!: User;
  mostrarMensajeCancelar!: boolean;
  mostrarMensajePagar!: boolean;
  mostrarComprado!:boolean;

  constructor(private carsService: CarsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.carsToSale = this.carsService.getCars();
    this.user = JSON.parse(localStorage.getItem('user')!);
    this.carsToSale = this.carsToSale.filter( car => car.Status != CarStatus.NOT_RESERVED)
  }

  buyCar(car: Car) {

  }

  payHalf(car: Car){

  }


}
