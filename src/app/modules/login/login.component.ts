import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Car, User } from 'src/app/model/models';
import Cars from '../../../assets/cars.json';
import Users from '../../../assets/users.json';
//import Users from '../../assets/users.json';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  horizontalPosition: any;
  loading!: boolean;
  showError!: boolean;
  cars = Cars as Car[];
  users = Users as User[];
  //users: Array<User> = Users as Array<User>;

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private router: Router) {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    //this.users = this.createUsers();
  }

  login() {
    if (this.form.valid) {
      if (this.hasCredentials()) {
        this.showSnackBar();
        this.redirect();
        this.setLocalStorage();
      }
      else {
        this.showErrorMessage();
      }
    }
    else {
      this.showErrorMessage();
    }
  }

  private showErrorMessage() {
    this.showError = true;
  }
  
  showSnackBar() {
    this.snackBar.open('Usuario y pasword correctos', '', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition
    });
    this.loading = true;
  }

  private redirect() {
    this.loading = true;
    setTimeout(() => { this.router.navigate(['main/search']) }, 1500);
  }

  private setLocalStorage() {
    let email = this.form.get("email")?.value;
    let user = this.users.find(user => user.Email == email) ?? new User;
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('cars', JSON.stringify(this.cars));
    localStorage.setItem('originalCars', JSON.stringify(this.cars));
  }

  private hasCredentials() {
    let email = this.form.get("email")?.value;
    let pass = this.form.get("password")?.value;
    return this.users.some(user => {
      return user.Email === email && user.Pass === pass;
    });
    //return true //Comentado para no tener que poner la cuenta todo el tiempo
  }

  changed() {
    this.showError = false;
  }

}
