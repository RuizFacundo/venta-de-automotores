import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BankAccountDialogComponent } from '../dialogs/bank-account-dialog/bank-account-dialog.component';
import { CreditCardDialogComponent } from '../dialogs/credit-card-dialog/credit-card-dialog.component';
import { MercadoPagoDialogComponent } from '../dialogs/mercado-pago-dialog/mercado-pago-dialog.component';



@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {

  roles = ["Vendedor", "Comprador"];
  paymentMethods = [{ Name: "Cuenta Bancaria", position: 0 },
                  { Name: "Mercado Pago", position: 1 },
                    { Name: "Tarjeta de Crédito", position: 2 }];
  loading!: boolean;
  form!: FormGroup;
  showError!: boolean;
  showField!:boolean;
  placeHolders = ['El nombre es un campo obligatorio',
    'El apellido es un campo obligatorio',
    'EL domicilio es un campo obligatorio',
    'EL DNI es un campo obligatorio',
    "La fecha de nacimieto es un campo obligatorio",
    "El mail es un campo obligatorio",
    "La contraseña es un campo obligatorio",
    "Debe seleccionar un tipo de usuario",
    "Calle es un campo obligatorio",
    "Altura es un campo obligatorio",
    "Localidad es un campo obligatorio"]

  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      bornDate: ['', Validators.required],
      dni: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      paymentMethod: ['', Validators.required],
      userRole: ['', Validators.required],
      addessNumber: ['', Validators.required],
      street: ['', Validators.required],
      location: ['', Validators.required],
      owner: ['', Validators.required],
    })
  }

  ngOnInit(): void {

  }

  setPlaceHolder(key: string, positionPLaceHolder: number): string {
    return (this.form.get(key)?.hasError('required') && this.form.get(key)?.touched) ? this.placeHolders[positionPLaceHolder] : ''
  }

  doRegistry() {
    localStorage.setItem('user', JSON.stringify(this.form.get('name')?.value));
    setTimeout(() => { this.router.navigate(['main/search']) }, 1500);
  }

  changed() {
    this.showError = false;
  }

  openDialog(position: number) {
    if (position == 1) {
      this.showField = true;
    }
    else {
      this.showField = false;
      let components = [
        BankAccountDialogComponent,
        MercadoPagoDialogComponent,
        CreditCardDialogComponent
      ]
      const dialogRef = this.dialog.open<CreditCardDialogComponent| MercadoPagoDialogComponent| BankAccountDialogComponent>(components[position]);
      dialogRef.afterClosed().subscribe(formResult => {
        if (formResult) {
          this.form.get('paymentMethod')?.reset();
        }
      })
    }
  }


}
