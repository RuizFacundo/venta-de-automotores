import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/models';
import { CarsService } from 'src/app/service/cars.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  user!: User;
  @ViewChild('mySelect') actionsSelect: any;

  constructor(private carService: CarsService, private router: Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!);
  }

  doFiltering(filter:string) {
    this.carService.doFilter(filter);
  }

}
