import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-mercado-pago-dialog',
  templateUrl: './mercado-pago-dialog.component.html',
  styleUrls: ['./mercado-pago-dialog.component.css']
})
export class MercadoPagoDialogComponent implements OnInit {

  form!: FormGroup;
  
  constructor(private fb:FormBuilder, public dialogRef: MatDialogRef<MercadoPagoDialogComponent>) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      user:['',Validators.required],
    });
    this.dialogRef.disableClose = true;
  }

  closeDialog(goBackPressed?: boolean){
    let invalid = goBackPressed ? goBackPressed : this.form.invalid;
    this.dialogRef.close(invalid);
  }

}
