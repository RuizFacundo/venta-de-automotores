import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-credit-card-dialog',
  templateUrl: './credit-card-dialog.component.html',
  styleUrls: ['./credit-card-dialog.component.css']
})
export class CreditCardDialogComponent implements OnInit {

  form!: FormGroup;
  
  constructor(private fb:FormBuilder, public dialogRef: MatDialogRef<CreditCardDialogComponent>) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      number:['',Validators.required],
      expirationDate:['',Validators.required],
      securityCode:['',Validators.required],

    });
    this.dialogRef.disableClose = true;
  }

  closeDialog(goBackPressed?: boolean){
    let invalid = goBackPressed ? goBackPressed : this.form.invalid;
    this.dialogRef.close(invalid);
  }

}
