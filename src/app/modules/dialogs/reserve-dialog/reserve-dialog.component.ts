import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Car } from 'src/app/model/models';

@Component({
  selector: 'app-reserve-dialog',
  templateUrl: './reserve-dialog.component.html',
  styleUrls: ['./reserve-dialog.component.css']
})
export class ReserveDialogComponent implements OnInit {

  randomSellers = ['Federico Lamuedra', '	Cristian Ciarallo', 'Cristiano Ronaldo', 'Pedro Pica Piedra', 'Roberto Carlos'];
  randomPrices = [50000000, 60000, 9000000, 80000000, 60000000];
  reserved!: boolean;

  constructor(
    public dialogRef: MatDialogRef<ReserveDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {car:Car, random:number}) { }

  ngOnInit(): void {
    this.dialogRef.updatePosition({ bottom: '10px'})
  }

  closeDialog(reserved: boolean){
    this.dialogRef.close(reserved);
  }

  showConfirmation(){
    this.reserved = true;
  }
}
