import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-bank-account-dialog',
  templateUrl: './bank-account-dialog.component.html',
  styleUrls: ['./bank-account-dialog.component.css']
})
export class BankAccountDialogComponent implements OnInit {

  form!: FormGroup;
  
  constructor(private fb:FormBuilder, public dialogRef: MatDialogRef<BankAccountDialogComponent>) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      cbu:['',Validators.required],
      owner:['',Validators.required],
      cuil:['',Validators.required],
    });
    this.dialogRef.disableClose = true;
  }

  closeDialog(goBackPressed?: boolean){
    let invalid = goBackPressed ? goBackPressed : this.form.invalid;
    this.dialogRef.close(invalid);
  }

}
