import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CarStatus } from 'src/app/model/card-status';
import { Car, User } from 'src/app/model/models';
import { CarsService } from 'src/app/service/cars.service';
import { ReserveDialogComponent } from '../dialogs/reserve-dialog/reserve-dialog.component';


@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.css'],
})
export class CarsListComponent implements OnInit {

  carsToSale!: Car[];
  user!: User;

  constructor(private carsService: CarsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!);
    this.carsToSale = this.carsService.getCars();
    this.carsToSale = this.carsToSale.filter( car => car.Status===0 || car.Status===2)
  }

  ngOnDestroy(){
    this.carsToSale = this.carsService.getOriginalCars();
  }


  bookCar(car: Car) {
    // Retorna un número aleatorio entre min (incluido) y max (excluido)
    let random = Math.round(Math.random() * (5 - 0) + 0);
    const dialogRef = this.dialog.open(ReserveDialogComponent,
      {
        data: { car: car, random: random },
      })
    dialogRef.afterClosed().subscribe(reserved => {
      if (reserved) {
        this.carsService.changeStatus(CarStatus.RESERVED, car.Id);
        window.location.reload();
      }
    });
  }

  getExchageMessage(hasExchange: boolean): string {
    return (hasExchange) ? 'SI' : 'NO';
  }

}
